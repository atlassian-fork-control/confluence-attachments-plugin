package it.jwebunit;

import com.atlassian.confluence.it.AcceptanceTestHelper;
import com.atlassian.confluence.it.Attachment;
import com.atlassian.confluence.it.Page;
import com.atlassian.confluence.it.Space;
import com.atlassian.confluence.it.SpacePermission;
import com.atlassian.confluence.it.User;
import com.atlassian.confluence.it.content.ViewContentBean;
import com.atlassian.confluence.it.rpc.ConfluenceRpc;
import com.atlassian.confluence.it.user.LoginHelper;
import org.junit.Before;
import org.junit.Test;
import org.junit.rules.TestName;

import java.io.IOException;

import static com.atlassian.confluence.it.AcceptanceTestHelper.TEST_SPACE;
import static net.sourceforge.jwebunit.junit.JWebUnit.assertLinkNotPresentWithExactText;
import static net.sourceforge.jwebunit.junit.JWebUnit.assertLinkPresentWithExactText;
import static net.sourceforge.jwebunit.junit.JWebUnit.getElementTextByXPath;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SpaceAttachmentsMacroTestCase {
    private AcceptanceTestHelper helper = AcceptanceTestHelper.make();
    private TestName testName = new TestName();
    private ConfluenceRpc rpc;
    private ConfluenceRpc adminRpc;
    private LoginHelper loginHelper;

    @Before
    public void setUp() throws Exception {
        helper.setUp(AttachmentsMacroTestCase.class, testName);
        rpc = ConfluenceRpc.newInstance(helper.getBaseUrl(), ConfluenceRpc.Version.V2_WITH_WIKI_MARKUP); //old school wikimarkup!
        adminRpc = ConfluenceRpc.newInstance(helper.getBaseUrl());
        loginHelper = new LoginHelper(helper.getWebTester());

        adminRpc.logIn(User.ADMIN);
        adminRpc.grantPermission(SpacePermission.VIEW, TEST_SPACE, User.TEST);
        adminRpc.grantPermission(SpacePermission.PAGE_EDIT, TEST_SPACE, User.TEST);
        adminRpc.grantPermission(SpacePermission.ATTACHMENT_CREATE, TEST_SPACE, User.TEST);
        rpc.logIn(User.TEST);
    }

    @Test
    public void testNoAttachmentMessageShownWhenThereIsNoAttachmentToSpace() {
        Page testPage = new Page(Space.TEST, "Page with space attachments macro", "{space-attachments:space=" + Space.TEST.getKey() + "}");
        rpc.createPage(testPage);
        adminRpc.flushIndexQueue();

        loginHelper.logInFast(User.TEST);
        ViewContentBean viewPage = ViewContentBean.viewPage(testPage);
        assertTrue(viewPage.getContent().contains("There are no attachments in the TST space."));
    }

    @Test
    public void testShowSpaceAttachments() throws IOException {
        Page pageWithMacro = new Page(Space.TEST, "Page with space attachments macro", "{space-attachments:space=" + Space.TEST.getKey() + "}");
        Page testPage = new Page(Space.TEST, "Page with attachments", "");

        Attachment attachment = new Attachment(testPage, "test1.txt", "text/plain", "", "Test content".getBytes("UTF-8"));

        rpc.createPage(testPage);
        rpc.createPage(pageWithMacro);
        rpc.createAttachment(attachment);
        adminRpc.flushIndexQueue();

        loginHelper.logInFast(User.TEST);
        ViewContentBean.viewPage(pageWithMacro);

        assertEquals(attachment.getFilename(),
                getElementTextByXPath("//div[contains(@class, 'attachments-container')]//table[contains(@class, 'attachments') and contains(@class, 'aui')]//tbody//tr[1]//td[1]//a[2]"));

        assertEquals(testPage.getTitle(),
                getElementTextByXPath("//div[contains(@class, 'attachments-container')]//table[contains(@class, 'attachments') and contains(@class, 'aui')]//tbody//td[contains(@class, 'attachedto')]//a"));

    }

    @Test
    public void testSpaceAttachmentsAttachedToCorrectPage() throws IOException {
        final String testContentString = "Test content";

        Page pageWithMacro = new Page(Space.TEST, "Page with space attachments macro", "{space-attachments:space=" + Space.TEST.getKey() + "}");
        Page testPage1 = new Page(Space.TEST, "Page 1", "");
        Page testPage2 = new Page(Space.TEST, "Page 2", "");

        Attachment attachment1 = new Attachment(testPage1, "test1.txt", "text/plain", "", testContentString.getBytes("UTF-8"));
        Attachment attachment2 = new Attachment(testPage2, "test2.txt", "text/plain", "", testContentString.getBytes("UTF-8"));

        rpc.createPage(testPage1);
        rpc.createPage(testPage2);
        rpc.createPage(pageWithMacro);
        rpc.createAttachment(attachment1);
        rpc.createAttachment(attachment2);
        adminRpc.flushIndexQueue();

        loginHelper.logInFast(User.TEST);
        ViewContentBean.viewPage(pageWithMacro);

        assertEquals("test2.txt", getElementTextByXPath("//div[contains(@class, 'attachments-container')]//table[contains(@class, 'attachments') and contains(@class, 'aui')]//tbody//tr[1]//td[1]//a[2]"));
        assertEquals("Page 2", getElementTextByXPath("//div[contains(@class, 'attachments-container')]//table[contains(@class, 'attachments') and contains(@class, 'aui')]//tbody//tr[1]//td[contains(@class, 'attachedto')]//a"));
        assertEquals("test1.txt", getElementTextByXPath("//div[contains(@class, 'attachments-container')]//table[contains(@class, 'attachments') and contains(@class, 'aui')]//tbody//tr[2]//td[1]//a[2]"));
        assertEquals("Page 1", getElementTextByXPath("//div[contains(@class, 'attachments-container')]//table[contains(@class, 'attachments') and contains(@class, 'aui')]//tbody//tr[2]//td[contains(@class, 'attachedto')]//a"));
    }

    @Test
    public void testAttachmentFromOtherSpaceNotShow() throws IOException {
        final String testContentString = "Test content";

        Space anotherSpace = new Space("AA", "Another");
        Page pageWithMacro = new Page(Space.TEST, "Page with space attachments macro", "{space-attachments:space=" + Space.TEST.getKey() + "}");
        Page testPage1 = new Page(Space.TEST, "Page 1", "");
        Page testPage2 = new Page(anotherSpace, "Page 2", "");

        Attachment attachment1 = new Attachment(testPage1, "test1.txt", "text/plain", "", testContentString.getBytes("UTF-8"));
        Attachment attachment2 = new Attachment(testPage2, "test2.txt", "text/plain", "", testContentString.getBytes("UTF-8"));

        rpc.createSpace(anotherSpace);
        rpc.createPage(testPage1);
        rpc.createPage(testPage2);
        rpc.createPage(pageWithMacro);
        rpc.createAttachment(attachment1);
        rpc.createAttachment(attachment2);
        adminRpc.flushIndexQueue();

        loginHelper.logInFast(User.TEST);
        ViewContentBean.viewPage(pageWithMacro);

        assertEquals(attachment1.getFilename(), getElementTextByXPath("//div[contains(@class, 'attachments-container')]//table[contains(@class, 'attachments') and contains(@class, 'aui')]//tbody//tr[1]//td[1]//a[2]"));
        assertEquals(testPage1.getTitle(), getElementTextByXPath("//div[contains(@class, 'attachments-container')]//table[contains(@class, 'attachments') and contains(@class, 'aui')]//tbody//tr[1]//td[contains(@class, 'attachedto')]//a"));
        assertLinkPresentWithExactText(attachment1.getFilename());
        assertLinkNotPresentWithExactText(attachment2.getFilename());
    }
}
