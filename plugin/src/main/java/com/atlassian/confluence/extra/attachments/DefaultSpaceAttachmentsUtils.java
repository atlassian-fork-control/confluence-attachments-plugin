package com.atlassian.confluence.extra.attachments;

import com.atlassian.annotations.VisibleForTesting;
import com.atlassian.bonnie.Handle;
import com.atlassian.confluence.core.persistence.AnyTypeDao;
import com.atlassian.confluence.macro.query.BooleanQueryFactory;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.search.v2.ContentSearch;
import com.atlassian.confluence.search.v2.ISearch;
import com.atlassian.confluence.search.v2.InvalidSearchException;
import com.atlassian.confluence.search.v2.SearchManager;
import com.atlassian.confluence.search.v2.SearchResult;
import com.atlassian.confluence.search.v2.SearchResults;
import com.atlassian.confluence.search.v2.SearchSort;
import com.atlassian.confluence.search.v2.filter.SubsetResultFilter;
import com.atlassian.confluence.search.v2.query.ContentTypeQuery;
import com.atlassian.confluence.search.v2.query.FileExtensionQuery;
import com.atlassian.confluence.search.v2.query.InSpaceQuery;
import com.atlassian.confluence.search.v2.query.LabelQuery;
import com.atlassian.confluence.search.v2.searchfilter.ContentPermissionsSearchFilter;
import com.atlassian.confluence.search.v2.sort.CreatedSort;
import com.atlassian.confluence.search.v2.sort.ModifiedSort;
import com.atlassian.confluence.search.v2.sort.RelevanceSort;
import com.atlassian.confluence.search.v2.sort.TitleSort;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class DefaultSpaceAttachmentsUtils implements SpaceAttachmentsUtils {
    private static final Logger LOG = LoggerFactory.getLogger(DefaultSpaceAttachmentsUtils.class);

    private final SearchManager searchManager;
    private final AnyTypeDao anyTypeDao;

    private int totalAttachments;
    private int totalPage;

    public DefaultSpaceAttachmentsUtils(SearchManager searchManager, AnyTypeDao anyTypeDao) {
        this.searchManager = searchManager;
        this.anyTypeDao = anyTypeDao;
    }

    public SpaceAttachments getAttachmentList(String spaceKey, int pageNumber, int previousTotalAttachments, int pageSize, String sortBy, String fileExtension, Set<String> labels) throws InvalidSearchException {
        SpaceAttachments spaceAttachments = new SpaceAttachments();
        BooleanQueryFactory query = new BooleanQueryFactory();
        query.addMust(new ContentTypeQuery(ContentTypeEnum.ATTACHMENT));
        query.addMust(new InSpaceQuery(spaceKey));

        if (isNotBlank(fileExtension)) {
            query.addMust(new FileExtensionQuery(fileExtension));
        }
        if (labels != null) {
            labels.stream().map(label -> new LabelQuery(label.trim())).forEach(query::addMust);
        }
        SearchSort searchSort = new RelevanceSort();
        if ("name".equalsIgnoreCase(sortBy)) {
            searchSort = new TitleSort(SearchSort.Order.ASCENDING);
        } else if ("date".equalsIgnoreCase(sortBy)) {
            searchSort = new ModifiedSort(SearchSort.Order.DESCENDING);
        } else if ("createddate".equalsIgnoreCase(sortBy)) {
            searchSort = new CreatedSort(SearchSort.Order.DESCENDING);
        }

        int startIndex = 0;
        if (pageSize == 0) {
            pageSize = COUNT_ON_EACH_PAGE;
        }
        if (previousTotalAttachments > 0) {
            startIndex = calculateStartIndex(pageNumber, pageSize);
        }

        ISearch search = new ContentSearch(query.toBooleanQuery(), searchSort, ContentPermissionsSearchFilter.getInstance(), new SubsetResultFilter(startIndex, pageSize));

        List<Attachment> attachmentList = new ArrayList<>();

        try {
            SearchResults searchResults = searchManager.search(search);
            totalAttachments = searchResults.getUnfilteredResultsCount(); // update the count again in case there are new attachments
            totalPage = calculateTotalPage(totalAttachments, pageSize);

            for (SearchResult searchResult : searchResults) {
                Handle handle = searchResult.getHandle();
                Attachment attachment = (Attachment) anyTypeDao.findByHandle(handle);
                attachmentList.add(attachment);
            }

            spaceAttachments.setAttachmentList(attachmentList);
            spaceAttachments.setTotalAttachments(totalAttachments);
            spaceAttachments.setTotalPage(totalPage);

        } catch (InvalidSearchException e) {
            LOG.error("Invalid search exception ", e);
            throw new InvalidSearchException(e.getMessage());
        }

        return spaceAttachments;
    }

    @VisibleForTesting
    protected int calculateTotalPage(int totalAttachments, int pageSize) {
        double dPageTotal = Math.ceil((double) totalAttachments / (double) pageSize);
        return (int) dPageTotal;
    }

    @VisibleForTesting
    protected int calculateStartIndex(int pageNumber, int pageSize) {
        return Math.max(0, (pageNumber - 1) * pageSize);
    }

}
